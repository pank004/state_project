import React from 'react';
import ReactDOM from 'react-dom/client';
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';



class App extends React.Component{

    state = {
        lat: null,
        errMessage:''
    };

    componentDidMount(){
        window.navigator.geolocation.getCurrentPosition(
            position => {this.setState({lat:position.coords.latitude})},
            err =>{this.setState({errMessage:err.message})}
        );
    }

    renderConfig(){

        if(this.state.lat && !this.state.errMessage){
            return<SeasonDisplay lat={this.state.lat}/>
        }
        if(!this.state.lat && this.state.errMessage){
        return(<div> err= {this.state.errMessage}</div>)
        }
        return <Spinner message='Please accept Request' />
    }

    render(){
        
        return(
            <div className='border-red'>

                {this.renderConfig()}
            </div>

        )
        
    }
}

const root = ReactDOM.createRoot(document.getElementById("root"));



root.render(<App></App>);